#!/bin/bash

set -exu

DEBUG=true
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

REPO_URL=https://salsa.debian.org/josch/fakeroot-foreign

TARGET=/srv/fakeroot-foreign

sudo mkdir -p $TARGET/
sudo chown -R jenkins:jenkins $TARGET/

if [ ! -d $TARGET/fakeroot-foreign ]; then
	git clone $REPO_URL $TARGET/fakeroot-foreign
else
	git -C $TARGET/fakeroot-foreign status || /bin/true
	git -C $TARGET/fakeroot-foreign fetch
	git -C $TARGET/fakeroot-foreign reset --hard origin/master
fi

sh $TARGET/fakeroot-foreign/run.sh

# vim: set sw=0 noet :
